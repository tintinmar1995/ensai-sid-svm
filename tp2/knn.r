library(class) 
# La librairie class contient knn.cv
# On aurait également pu utiliser une fonction de la library caret

knn.learn <- function(data, ks = c(1, 3, 5, 9)){
  # ks <- 1:(10*( (knn$nrow(data))^(1/(ncol(data) - 1)) ) + 1)
  knn <- structure(
    list(k = 0,
         ks = ks, 
         train <- data[,1:(ncol(data) - 1)], 
         cl <- data[,ncol(data)]),
    .Names = c("k", "ks", "train", "cl"))
  knn$k <- knn.bestk.cv(train, cl, ks)
  return(knn)
}

knn.classify <- function(knn, objects){
  return (knn(knn$train, objects, knn$cl, knn$k))
}

# Routines #####################################################################
knn.bestk.cv <- function(train, cl, ks){
  cv.err <- cl.pred <- c()
  for (i in ks){
    newpre <- as.vector(knn.cv(train, cl, k = i))
    cl.pred <- cbind(cl.pred, newpre)
    cv.err <- c(cv.err, sum(cl != newpre))
  } 
  k0 <- ks[which.min(cv.err)] 
  return (k0)
}
# Routines (end )###############################################################
