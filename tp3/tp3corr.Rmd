---
title: 'TP3 : MNIST'
author: "fabien.navarro@ensai.fr"
date: "`r Sys.Date()`"
output:
  rmarkdown::html_document:
    theme: cerulean
    number_sections: no
    toc: yes
    toc_depth: 5
    toc_float: true
---
<style type="text/css">

body{ /* Normal  */
      font-size: 16px;
  }
td {  /* Table  */
  font-size: 8px;
}
h1.title {
  font-size: 38px;
  color: DarkRed;
}
h1 { /* Header 1 */
  font-size: 28px;
  color: DarkBlue;
}
h2 { /* Header 2 */
    font-size: 18px;
  color: DarkBlue;
}
h3 { /* Header 3 */
  font-size: 15px;
  font-family: "Times New Roman", Times, serif;
  color: DarkBlue;
}
code.r{ /* Code block */
    font-size: 12px;
}
pre { /* Code block - determines code spacing between lines */
    font-size: 14px;
}
</style>


La base de données MNIST est une base bien connue d'images de chiffres manuscrits (0-9) qui est utilisée pour évaluer les algorithmes d'apprentissage automatique. La base contient 60 000 images d'entraînement et 10 000 images de test. Les données MNIST sont stockées dans 4 fichiers binaires, ce qui peut être difficile à gérer directement. Télécharger les données (sur moodle ou directement sur le site de Yann LeCun [ici](http://yann.lecun.com/exdb/mnist/index.html)) et vous pouvez utiliser le code ci-dessous pour les charger sous `R`. En python, elles sont accessible via l'API de tensorflow par exemple.

```{python , eval=FALSE}
import tensorflow as tf
(x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data()
```

Les données MNIST sont transférés sous format binary... Cela nécessite une importation particulière.  

```{r}
nTrain <- readBin(con = "mnistdataset/train-images-idx3-ubyte", what = "integer", n = 4, 
                  size = 4, endian = "big")[2]
nTest <- readBin(con = "mnistdataset/t10k-images-idx3-ubyte", what = "integer", n = 4, 
                 size = 4, endian = "big")[2]
nRows <- readBin(con = "mnistdataset/train-images-idx3-ubyte", what = "integer", n = 4, 
                 size = 4, endian = "big")[3]
nCols <- readBin(con = "mnistdataset/train-images-idx3-ubyte", what = "integer", n = 4, 
                 size = 4, endian = "big")[4]

# Load training data
images.train <- array(as.numeric(readBin(con = "mnistdataset/train-images-idx3-ubyte", 
                                         what = "raw", 
                                         n = nTrain * nRows * nCols + 16, 
                                         endian = "big")[-(1:16)]), 
                      dim = c(nCols, nRows, nTrain))

labels.train <- as.numeric(readBin(con = "mnistdataset/train-labels-idx1-ubyte", 
                                   what = "raw", 
                                   n = nTrain + 8, 
                                   endian = "big")[-(1:8)])
# Load test data
images.test <- array(as.numeric(readBin(con = "mnistdataset/t10k-images-idx3-ubyte", 
                                        what = "raw", 
                                        n = nTest * nRows * nCols + 16, 
                                        endian = "big")[-(1:16)]), 
                     dim = c(nCols, nRows, nTest))
labels.test <- as.numeric(readBin(con = "mnistdataset/t10k-labels-idx1-ubyte", 
                                  what = "raw", 
                                  n = nTest + 8, 
                                  endian = "big")[-(1:8)])
```

# 1. Affichier les 10 premières images de la base d'apprentissage. Vous pouvez utiliser les fonctions `image` et `grey` ou alors le pacakge `imager` (en les convertissant au préalable comme des objets cimg). Avec Python matplotlib.
```{r}
image.rot <- function(m){
	t(m)[,nrow(m):1]
} 

# Définition d'une plage de couleurs pour afficher l'image
greycol <- rev(grey((0:255)/255))
#greycol <- grey((0:255)/255) 

# Comment sont stockées les images ?
# images.train = ensemble de matrice. Chaque matrice représente la couleur du pixel (en nuance de gris)

image(images.train[,,3], col  = greycol)
```

```{r}
for (i in 1:10){
  image(images.train[,,i], col  = greycol)
}
```

# 2. Même question pour les 10 premières images de la base de test
```{r}
for (i in 1:10){
  image(images.test[,,i], col  = greycol)
}
```

# 3. Dans un premier temps, réduire la taille de la base (pour des raisons de temps de calculs). Créer un sous ensemble aléatoire de la base d'apprentissage de taille 30 fois plus petite. On se restraint également à 3 classes (0,1,2).
```{r}
set.seed(1)
train.subsample <- sample(1:nTrain, nTrain / 30)
nTrain.sub <- length(train.subsample)
digits <- 0:2
```

# 4. Préparer les données en vue d'appliquer une SVM (les normaliser). Vous pouvez par exemple mettre les données d'apprentissage dans une matrice (de dimension adaptée).  
```{r}
source("rescale.R")
X.train <- images.train[,,train.subsample]
dim(X.train) = c(nRows * nCols, nTrain.sub)
X.train <- t(X.train)
#X.train <- rescale(X.train,0,1)
X.train <- X.train/ 255 * 2 - 1             # On normalise entre -1 et 1 
Y.train <- labels.train[train.subsample]
X.train <- X.train[Y.train %in% digits,]
Y.train <- Y.train[Y.train %in% digits]
```

# 6. Entraîner une SVM sur les données d'apprentissage (e.g. vous pouvez utiliser la fonction `svm` du package `e1071` sous `R`, `sklearn.svm.SVC` sous python). Encore une fois, pour des raisons de temps CPU, on fixe les paramètres a priori dans un premier temps. Utiliser un noyau gaussien $K(u,v)$
```{r}
machine.svm <-  e1071::svm(x = X.train, 
                           y = Y.train, 
                           type = "C-classification", 
                           kernel = "radial", 
                           best.gamma = 0.01, 
                           best.cost = 1, 
                           scale = F)
```

# 7. Réduire la taille des données de tests (par exemple ne conserver que les 1000 premières images) et préparer les de la même manière que les données d'apprentissages.
```{r}
nTest <- 1000
X.test <- images.test[,,1:nTest]
dim(X.test) = c(nRows * nCols, nTest)
X.test <- t(X.test)
#X.test <- rescale(X.test,0,1)
X.test <- X.test/ 255 * 2 - 1
Y.test <- labels.test[1:nTest]
X.test <- X.test[Y.test %in% digits,]
Y.test <- Y.test[Y.test %in% digits]

response <- predict(object = machine.svm, 
                    newdata = X.test)
```

# 8. Calculer l'erreur de prediction de la svm (sur les trois premières classes uniquement).
```{r}
cat("L'erreur de classification (calculer sur ", nrow(X.test), 
    " observations avec ", length(digits), " digits) est: ", 
    mean(response != Y.test), "\n", sep = "")
```

# 9. Afficher la matrice de confusion.
```{r}
conf <- matrix(NA, nrow = length(digits), ncol = length(digits))
for (i in 1:length(digits)){
  conf[i,] <- table(response[Y.test %in% digits[i]])
  conf[i,] <- conf[i,] / sum(conf[i,])
}
cat("La matrice de confusion correspondante:\n")
conf <- data.frame(conf)
colnames(conf) <- paste(digits)
rownames(conf) <- paste(digits)
print(round(conf,4))
```

# 10. Comparer les résultats en utilisant la méthode de classification de votre choix (e.g. un CNN).
```{r}
# voir par exemple ici pour une comparaison avec un CNN
#https://towardsdatascience.com/image-classification-in-10-minutes-with-mnist-dataset-54c35b77a38d
```

# 11. Tester les performances de la SVM en considérant d'autres classes (plus difficile à distinguer a priori et en augmentant leurs nombres). Vous pouvez également comparer vos résultats avec un autre classifieur.
```{r}

```

# 12. En utilisant les pacakges [foreach](https://cran.r-project.org/web/packages/foreach/index.html) et [doMC](https://cran.r-project.org/web/packages/doMC/index.html) (vous pouvez consulter les vignettes associées [foreachvignettes](https://cran.r-project.org/web/packages/foreach/vignettes/foreach.pdf) ou encore [doMCvignettes associées](https://cran.r-project.org/web/packages/doMC/vignettes/gettingstartedMC.pdf)) paralléliser l'étape de calibration par K-fold cross-validation des paramètres $\gamma$ et `cost` de la SVM. On prendra par exemple $\gamma=cost=10^{-3:3}$ et $K=5$. En python, le module [multiprocessing](https://docs.python.org/2/library/multiprocessing.html), et en particulier la classe `Pool` (avec ses méthodes associées, .apply, .map). Cette étape peut-être réalisée via le cluster (pour `R` uniquement).
```{r}
library(foreach)
library(doMC)
library(abind)
library(ggplot2)
registerDoMC(4)
gammas <- 10^(-2:2)
costs <- 10^(-2:2)
n.cross <- 5
res <- foreach(gamma = gammas, .combine = function(x, y){
  abind(x, y, along = 3)}) %dopar% {
  foreach(cost = costs, .combine = cbind) %dopar% {
    foreach(i = 1:n.cross, .combine = c) %dopar% {
      i.sub <- 0:floor(nrow(X.train) / n.cross) * n.cross + i
      i.sub <- i.sub[i.sub < nrow(X.train)]
      svm.tmp <- svm(x = X.train[-i.sub,], 
                     y = Y.train[-i.sub], 
                     type = "C-classification", 
                     kernel = "radial", 
                     gamma = gamma, 
                     cost = cost, 
                     scale = F)
      sum(predict(object = svm.tmp, 
                  newdata = X.train[i.sub,]) != Y.train[i.sub])
    }
  }
}
svm.cv.errors <- apply(res, c(2,3), sum) / nrow(X.train)
best.gamma <- gammas[floor((which.min(svm.cv.errors) - 1) / length(costs)) + 1]
best.cost <- costs[(which.min(svm.cv.errors) - 1) %% length(costs) + 1]
cv.errors <- as.vector(t(svm.cv.errors))
cv.grid <- expand.grid(gamma = paste(gammas), cost = paste(costs))
ggplot(data = cv.grid, aes(x = cost, y = gamma)) +
geom_tile(aes(fill = cv.errors))
```
